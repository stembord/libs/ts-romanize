const LOOKUP_TABLE: { [key: string]: number } = {
  M: 1000,
  CM: 900,
  D: 500,
  CD: 400,
  C: 100,
  XC: 90,
  L: 50,
  XL: 40,
  X: 10,
  IX: 9,
  V: 5,
  IV: 4,
  I: 1
};

const SEARCH_KEYS_ORDERED: string[] = [
  "M",
  "CM",
  "D",
  "CD",
  "C",
  "XC",
  "L",
  "XL",
  "X",
  "IX",
  "V",
  "IV",
  "I"
];

export function toRomanNumber(x: number) {
  let romanNumber = "";

  for (const index in SEARCH_KEYS_ORDERED) {
    if (SEARCH_KEYS_ORDERED.hasOwnProperty(index)) {
      const romanNumberSymbolKey: string = SEARCH_KEYS_ORDERED[index];
      while (x >= LOOKUP_TABLE[romanNumberSymbolKey]) {
        romanNumber += romanNumberSymbolKey;
        x -= LOOKUP_TABLE[romanNumberSymbolKey];
      }
    }
  }

  return romanNumber;
}
