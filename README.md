# ts-react-quiz

react quiz

```json
"dependencies": {
  "@stembord/romanize": "git://gitlab.com/stembord/libs/ts-romanize.git"
}
```

```tsx
import { toRomanNumber } from "@stembord/romanize";

toRomanNumber(4); // returns 'IV'
```
