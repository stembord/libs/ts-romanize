import * as test from "tape";
import { toRomanNumber } from "../lib";

interface ITestItem {
  number: number;
  roman: string;
  msg: string;
}

test("toRomanNumber", (assert: test.Test) => {
  const testItems: ITestItem[] = [
    { number: 1, roman: "I", msg: "1 should equal I" },
    { number: 2, roman: "II", msg: "2 should equal II" },
    { number: 3, roman: "III", msg: "3 should equal III" },
    { number: 4, roman: "IV", msg: "4 should equal IV" },
    { number: 5, roman: "V", msg: "5 should equal V" },
    { number: 6, roman: "VI", msg: "6 should equal VI" },
    { number: 7, roman: "VII", msg: "7 should equal VII" },
    { number: 8, roman: "VIII", msg: "8 should equal VIII" },
    { number: 9, roman: "IX", msg: "9 should equal IX" },
    { number: 10, roman: "X", msg: "10 should equal X" }
  ];

  testItems.forEach(testItem => {
    const result = toRomanNumber(testItem.number);
    assert.equal(result, testItem.roman, testItem.msg);
  });

  assert.end();
});
